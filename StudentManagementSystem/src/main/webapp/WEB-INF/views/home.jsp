<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Student management system</title>
<!-- Bootstrap cdn -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <!-- Datatable -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>

	
</head>
<body style="background-color:#D6EAF8;">

 <%@ include file="header.jsp" %>
 
 <style>

 .student{
    margin: 0 auto;
    width: 70%;
  
}
h2 {
    margin: 5px 0 0;
    color: blue;
    font-size: 40px;
    text-align:center;
}
.btn{
    min-width: 50px;
    color: white;
    font-size: 15px;
    border-radius: 1px;
  
    margin-left: 20px;
}



 .modal {
    position: absolute;
    top: 10px;
    right: 100px;
    bottom: 0;
    left: 0;
    z-index: 10040;
    overflow: auto;
    overflow-y: auto;
 }

 </style>

 
  <div class="row">
<table id="studentList" class="table table-striped table-bordered" >

    
		<h3 align="center">Student List</h3>
				
					<thead>
						<tr>
							<th rowspan="2">ID</th>
							<th rowspan="2">Name</th>
							<th rowspan="2">Email</th>
							<th rowspan="2">Address</th>
							<th rowspan="2">Phone</th>
							<th colspan="2" style="text-align: center">Action</th>
						</tr>
						<tr>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>

				</table>
			</div>
	
	


            
       
            <!-- add Modal HTML -->
            <div class="container">
       
                <div class="row">
            <div class="modal fade modal-content modal-dialog " tabindex="-1" role="dialog" id="addStudentModal" role="document">
		
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Add Student</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
					</div>
					<div class="modal-body">	
								
						<div class="form-group">
							<label>Name</label>
							<input type="text" id="name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="text" id="email" class="form-control"required >
						</div>
						<div class="form-group">
							<label>Address</label>
							<input type="text" class="form-control" id="address"required >
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="text" class="form-control" id="phone"required >
						</div>					
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-danger" data-dismiss="modal" value="Cancel">
						<button type="submit" id="add" class="btn btn-success" data-dismiss="modal" onclick="insert()" >Add</button>
					</div>
				</form>
			</div>
		</div>
    </div>
   <!-- Edit Modal HTML -->
	
        <div class="container">
            <div class="wrapper-editor">
            <div class="row">
        <div class="modal fade modal-content modal-dialog modalWrapper " tabindex="-1" role="dialog" id="editStudentModal" role="document">
					<div class="modal-header">						
						<h5 class="modal-title">Edit Student</h5>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">	
					<div class="form-group" >
							<label>ID</label>
							<input type="text"  id="editID"  class="form-control" >
						</div>				
						<div class="form-group" >
							<label>Name</label>
							<input type="text"  id="editName"class="form-control" >
						</div>
						<div class="form-group" >
							<label>Email</label>
							<input type="text"  id="emailEdit" class="form-control" required>
						</div>
						<div class="form-group" >
							<label>Address</label>
							<input type="text" id="addressEdit" class="form-control" required>
							</div>
						<div class="form-group" >
							<label>Phone</label>
							<input type="text" id="phoneEdit" class="form-control" required>
						</div>					
					</div>
					
					<div class="modal-footer">
							<input type="button" class="btn btn-danger" data-dismiss="modal" value="Cancel">

							<input type="button" class="btn btn-success" id="update" value="Update" name="Update" />
						</div>
                </div>
			</div>
		</div>
	</div>
<div class="footer">
		<ul class="list-unstyled list-inline text-center">
			<li class="list-inline-item">

				<i class="fa fa-facebook-square" style="font-size:24px"></i>

			</li>
			<li class="list-inline-item">
				<i class="fa fa-linkedin-square" style="font-size:24px"></i>
			</li>
			<li class="list-inline-item">
				<i class="fa fa-twitter-square" style="font-size:24px"></i>
			</li>

		</ul>
		<div class="footer-copyright text-center py-3">� 2020 Copyright:
			<a href="#"> Surabhi Regmi</a>
		</div>
	</div>


<script>

var A_PAGE_CONTEXT_PATH = "${pageContext.request.contextPath}"
	/* For Adding Data */
	function insert(){
	
		var studentObj = {
			name : $("#name").val(),
			email : $("#email").val(),
			address :  $("#address").val(),
			phone : $("#phone").val(),
		};
		console.log("student",studentObj);
		if(studentObj.name=="" || studentObj.email=="" ||  studentObj.address==""||studentObj.phone==""){
		       alert("All Fields are required to be filled!!!");
		    }
		else{
		alert("save");
		$.ajax({
			method : "POST",
			url : A_PAGE_CONTEXT_PATH + '/save',
			data : JSON.stringify(studentObj),
			dataType : "json",
			contentType : "application/json",
			cache : false,
			success : function(data, textStatus, xhr) {
				alert("success");
				/* if(data=="ok"){
				alert("success");
				}
				else 
					{
					alert(data);
					 $('input[type="text"], textarea').val('');
					    $('input[type="email"], textarea').val('');
					    $('#editStudentModal').reload();
					}
				  */
				
			},
			error:function (data, textStatus, xhr) {
			
		        alert("status"+textStatus);
		        
		      },
		    
		 	complete : function(response) {
				 $("addStudentModal").modal('hide'); 
				location.reload();
				SequentialExecutor.executeNext();
			} 
			
		});
	}
}
/*  Update Data */
$("#update").click(function() {
	 var editObj = {
		id : $("#editID").val(),
		name : $("#editName").val(),
		email : $("#emailEdit").val(),
		address:$("#addressEdit").val(),
		phone : $("#phoneEdit").val()
	};
	
	 console.log("editObj",editObj);
	 alert(editObj.id+" name="+editObj.name);
	$.ajax({
		method : "POST",
		url : A_PAGE_CONTEXT_PATH +'/updateStudent',
		data : JSON.stringify(editObj),
		dataType : "json",
		contentType : "application/json",
		cache : false,
		success : function(data, textStatus, xhr) {
			alert("Update Successful");
		},error:function(data, textStatus, xhr){
	alert("error");	},		
		complete : function(response) {
			$("#editStudentModal").modal('hide');
			location.reload();
			SequentialExecutor.executeNext();
		}
	});
});


function onEdit(ctl) {

    id = parseInt($(ctl).attr("id").replace("edit", ""));
    alert(id);
    $.ajax({
		method : "GET",
		url : A_PAGE_CONTEXT_PATH + '/edit/'+id,
		dataType : "json",
		contentType : "application/json",
		cache : false,
		success : function(response) {
			console.log("response",response);
			$("#editID").val(response.id);
			$("#editName").val(response.name);
			 $("#emailEdit").val(response.email);
			 $("#addressEdit").val(response.address);
			 $("#phoneEdit").val(response.phone);
		}, error:function(xhr,textStatus){
	alert("error"+textStatus);			
		}
		
	});
}

function onDelete(clt){
	
	delID=parseInt($(clt).attr("id").replace("del",""));
	//alert(delID);
	var r = confirm("Are you sure?");
	if (r == true){
	$.ajax({
		method : "POST",
		url : A_PAGE_CONTEXT_PATH+'/deleteStudent/'+delID,
		cache : "false",
		success : function(data, textStatus, xhr){
			alert("Data Deleted successfully");
			
		},		
		complete : function(response) {
			$("#editStudentModal").modal('hide');
			location.reload();
			SequentialExecutor.executeNext();
		} 
		
		
	})
	
}
}

/* DAta Table*/


	$('#studentList').DataTable(
					{
						destroy: true,
						 scrollY:        300,
					        scrollCollapse: true,
					        scroller:       true,
						"ajax" : {
						"url" : A_PAGE_CONTEXT_PATH + '/studentList',
						dataSrc:""
						
							
						},

						"columns" : [
								{
									"data" : "id",
									render: function (data, type, row, meta) {
								        return meta.row + meta.settings._iDisplayStart + 1;
								    }
								},
								{
									"data" : "name"
								},
								{
									"data" : "email"
								},

								{
									"data" : "address"
								},
								
								{
									"data" : "phone"							
								},
								{
					                data: function (param_obj, type, full, meta, oData) {
					                    return '<button type="button" style="color:green"  id="edit' + param_obj.id + '" data-toggle="modal" data-target="#editStudentModal"  onclick="onEdit(this)" >' + '<span class="glyphicon  glyphicon-pencil"/>' + '</button>';
					                }
					            },
					            {
					                data: function (param_obj, type, full, meta, oData) {
					                    return '<button type="button" style="color:red"  id="del' + param_obj.id + '"   onclick="onDelete(this)" >' + '<span class="glyphicon  glyphicon-remove"/>' + '</button>';
					                }
					            } ]
					});
	
	
	



</script>
</html>