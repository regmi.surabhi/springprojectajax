<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link  rel="stylesheet" href="/resources/css/bootstrap.min.css">
	
    
    <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.css">
    
    <script  type="text/javascript" src="/resources/js/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="/resources/js/dataTables.bootstrap.js"></script>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body>
 
		<!-- Start navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">SMI System</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="home">Home</a></li>
            <li class="active" ><a data-toggle="modal"  data-dismiss="modal"  href=" #addStudentModal">Add Student</a></li> 
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="logout">Logout<span class="sr-only">(current)</span></a></li>
          </ul>
          <h2>Student Management System</h2>
          
        </div> 
      </div>
    </nav>
    <!-- End navbar -->
</body>
</body>
</html>