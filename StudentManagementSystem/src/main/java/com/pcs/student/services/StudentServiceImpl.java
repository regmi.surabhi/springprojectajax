package com.pcs.student.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pcs.student.dao.Studentdao;
import com.pcs.student.model.Student;
@Service
public class StudentServiceImpl implements StudentServices {
	@Autowired
	Studentdao sdao;

	@Transactional
	public List<Student> studentList() {
		// TODO Auto-generated method stub
		return sdao.studentList();
	}

	@Transactional
	public Student saveStudent(Student student) {
		 sdao.saveStudent(student);
		 return null;
	}

	@Transactional
	public Student updateStudent(Student student) {
		System.out.println("hibernate update");
		return sdao.updateStudent(student);
	}

	@Transactional
	public Student getStudent(int id) {
		return sdao.getStudent(id);
	}
	@Transactional
	public Student deleteStudent(int id) {
		return sdao.deleteStudent(id);
	
	}
}
