package com.pcs.student.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pcs.student.model.Student;
import com.pcs.student.services.StudentServices;
@Controller
public class StudentController {
	
		
		@Autowired
		StudentServices studentService;

		

			
		@RequestMapping(value="/save", method = RequestMethod.POST)
		public ResponseEntity saveStudent(@RequestBody Student student){
			System.out.println("inside save controller");
			 studentService.saveStudent(student);
			 return new ResponseEntity<Student>(student, HttpStatus.OK);
		}
		
		@RequestMapping(value="/studentList", method=RequestMethod.GET)
		public  @ResponseBody List<Student> getAllStudent(){
			System.out.println("inside student list");
			return studentService.studentList();
		}
		
		@RequestMapping(value = "/edit/{id}")
		public @ResponseBody Student editStudent(@PathVariable ("id") int id) {
			
			Student s= studentService.getStudent(id);
			System.out.println("edit student "+s.getAddress());
			return s;
			
		}
		 
		@RequestMapping(value = "/updateStudent", method = RequestMethod.POST)
		public ResponseEntity updateStudent(@RequestBody Student student) {
					System.out.println("TO update value=" + student.getName() + "\n" + student.getAddress() + "\n"
						+ student.getEmail() + student.getPhone()+" where id =" + student.getId());
				 studentService.updateStudent(student);
				//System.out.println("updated student=" + s);
				return new ResponseEntity<Student>(student, HttpStatus.OK);
			
		}
		@RequestMapping(value="studentList/{id}")
		public @ResponseBody Student studentListById(@PathVariable("id") int id){
			return studentService.getStudent(id);
		}
		@RequestMapping( "/deleteStudent/{delID}")
		public ResponseEntity deleteStudentById(@PathVariable("delID") int delID) {
			System.out.println("deleted"+studentService.deleteStudent(delID));
			 studentService.deleteStudent(delID);
			 return new ResponseEntity<Student>(HttpStatus.OK);

				
		}

		
	}


