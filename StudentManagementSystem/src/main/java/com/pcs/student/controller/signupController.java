package com.pcs.student.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.pcs.student.dao.userdao;
import com.pcs.student.model.User;
@Controller
public class signupController {
	@Autowired
	private userdao udao;
	@RequestMapping(value="/signup",method=RequestMethod.GET)
	public String getsignupform() {
		return"signup";
		

}
	@RequestMapping(value="/signup",method=RequestMethod.POST)
	public String saveUser(@ModelAttribute User user) {
		user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
		udao.signup(user);
		return "login";
	}
}
