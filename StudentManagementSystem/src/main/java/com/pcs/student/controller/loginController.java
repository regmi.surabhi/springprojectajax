package com.pcs.student.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.pcs.student.dao.userdao;
import com.pcs.student.model.User;

@Controller
public class loginController {
	@Autowired
	private userdao udao;
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String getloginform() {
		return"login";
		
	}
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String userlogin(@ModelAttribute User user,Model model) {
		//modelaatribute joins jsp with pojo
		
		user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
		User u=udao.login(user.getUsername(), user.getPassword());
		if(u!=null) {
			model.addAttribute("user",user.getUsername());
			return "home";
		}
			 
	
	model.addAttribute("error","user does not exist!");
	return "login";
	
}
	@RequestMapping(value="/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "login";
	}
}