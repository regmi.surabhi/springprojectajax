package com.pcs.student.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pcs.student.model.Student;
@Repository
public class StudentDaoImpl implements Studentdao {
	@Autowired
	private SessionFactory sessionFactory;
	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private Session getSession() {

		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	@Override
	public List<Student> studentList() {
		return getSession().createCriteria(Student.class).list();
	}

	@Override
	public Student saveStudent(Student student) {
		System.out.println("inside studentdaoimpl");
		 getSession().save(student); 
		 return null;
	}

	@Override
	public Student getStudent(int id) {
		return (Student) getSession().get(Student.class,id);
	}

	@Override
	public Student updateStudent(Student student) {
		getSession().saveOrUpdate(student);
		return null;
	}

	@Override
	public Student deleteStudent(int id) {
		Student student=getStudent(id);
		if(null!= student){
			getSession().delete(student);
		}
		return null;
		}

}
