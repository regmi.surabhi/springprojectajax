package com.pcs.student.dao;

import java.util.List;

import com.pcs.student.model.Student;

public interface Studentdao {
	public List<Student> studentList();
	public Student saveStudent(Student student);
	public Student getStudent(int id);
	public Student updateStudent(Student student);
	public Student deleteStudent(int id);

}
