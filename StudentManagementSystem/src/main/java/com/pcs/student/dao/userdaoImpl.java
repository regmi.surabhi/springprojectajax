package com.pcs.student.dao;


	import javax.annotation.Resource;
	import javax.annotation.Resources;

	import org.hibernate.Criteria;
	import org.hibernate.Session;
	import org.hibernate.SessionFactory;
	import org.hibernate.criterion.Restrictions;
	import org.springframework.stereotype.Repository;
	import org.springframework.transaction.annotation.Transactional;

import com.pcs.student.model.User;

	
	@Repository
	public class userdaoImpl implements userdao {

		
		@Resource
		private SessionFactory sessionFactory;//binding hibernate bean

		


		@Override
		@Transactional
		public User login(String un, String psw) {
			Session sess= sessionFactory.getCurrentSession();
			Criteria crt=sess.createCriteria(User.class);
			crt.add(Restrictions.eq("username", un));
			crt.add(Restrictions.eq("password", psw));
			
			return (User)crt.uniqueResult();
		}
	
	@Override
	@Transactional
	public void signup(User user) {
		Session sess= sessionFactory.getCurrentSession();
		sess.save(user);
	}
	}
		